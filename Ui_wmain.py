# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\RP\code\testpython\py2exe\wmain.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        Dialog.setSizeGripEnabled(True)
        self.layoutWidget = QtWidgets.QWidget(Dialog)
        self.layoutWidget.setGeometry(QtCore.QRect(30, 10, 341, 231))
        self.layoutWidget.setObjectName("layoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)
        self.leScript = QtWidgets.QLineEdit(self.layoutWidget)
        self.leScript.setObjectName("leScript")
        self.gridLayout.addWidget(self.leScript, 1, 0, 1, 1)
        self.leTargetDir = QtWidgets.QLineEdit(self.layoutWidget)
        self.leTargetDir.setObjectName("leTargetDir")
        self.gridLayout.addWidget(self.leTargetDir, 5, 0, 1, 1)
        self.cbUseWin = QtWidgets.QCheckBox(self.layoutWidget)
        self.cbUseWin.setObjectName("cbUseWin")
        self.gridLayout.addWidget(self.cbUseWin, 6, 0, 1, 1)
        self.btnScript = QtWidgets.QPushButton(self.layoutWidget)
        self.btnScript.setObjectName("btnScript")
        self.gridLayout.addWidget(self.btnScript, 1, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 7, 0, 1, 1)
        self.btnIcon = QtWidgets.QPushButton(self.layoutWidget)
        self.btnIcon.setObjectName("btnIcon")
        self.gridLayout.addWidget(self.btnIcon, 8, 1, 1, 1)
        self.btnTargetDir = QtWidgets.QPushButton(self.layoutWidget)
        self.btnTargetDir.setObjectName("btnTargetDir")
        self.gridLayout.addWidget(self.btnTargetDir, 5, 1, 1, 1)
        self.cbUseDefDir = QtWidgets.QCheckBox(self.layoutWidget)
        self.cbUseDefDir.setObjectName("cbUseDefDir")
        self.gridLayout.addWidget(self.cbUseDefDir, 4, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.leIcon = QtWidgets.QLineEdit(self.layoutWidget)
        self.leIcon.setObjectName("leIcon")
        self.gridLayout.addWidget(self.leIcon, 8, 0, 1, 1)
        self.leTargetName = QtWidgets.QLineEdit(self.layoutWidget)
        self.leTargetName.setObjectName("leTargetName")
        self.gridLayout.addWidget(self.leTargetName, 3, 0, 1, 1)
        self.btnOpen = QtWidgets.QPushButton(Dialog)
        self.btnOpen.setGeometry(QtCore.QRect(41, 254, 75, 23))
        self.btnOpen.setObjectName("btnOpen")
        self.btnSave = QtWidgets.QPushButton(Dialog)
        self.btnSave.setGeometry(QtCore.QRect(157, 254, 75, 23))
        self.btnSave.setObjectName("btnSave")
        self.btnGenerate = QtWidgets.QPushButton(Dialog)
        self.btnGenerate.setGeometry(QtCore.QRect(274, 254, 75, 23))
        self.btnGenerate.setObjectName("btnGenerate")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.btnOpen, self.btnSave)
        Dialog.setTabOrder(self.btnSave, self.btnGenerate)
        Dialog.setTabOrder(self.btnGenerate, self.leScript)
        Dialog.setTabOrder(self.leScript, self.btnScript)
        Dialog.setTabOrder(self.btnScript, self.leTargetName)
        Dialog.setTabOrder(self.leTargetName, self.cbUseDefDir)
        Dialog.setTabOrder(self.cbUseDefDir, self.leTargetDir)
        Dialog.setTabOrder(self.leTargetDir, self.btnTargetDir)
        Dialog.setTabOrder(self.btnTargetDir, self.cbUseWin)
        Dialog.setTabOrder(self.cbUseWin, self.leIcon)
        Dialog.setTabOrder(self.leIcon, self.btnIcon)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "py文件转exe生成器"))
        self.label_3.setText(_translate("Dialog", "可执行文件名:"))
        self.cbUseWin.setText(_translate("Dialog", "使用窗体"))
        self.btnScript.setText(_translate("Dialog", "浏览"))
        self.label_2.setText(_translate("Dialog", "图标:"))
        self.btnIcon.setText(_translate("Dialog", "浏览"))
        self.btnTargetDir.setText(_translate("Dialog", "浏览"))
        self.cbUseDefDir.setText(_translate("Dialog", "使用默认输出目录"))
        self.label.setText(_translate("Dialog", "主代码:"))
        self.btnOpen.setText(_translate("Dialog", "打开"))
        self.btnSave.setText(_translate("Dialog", "保存"))
        self.btnGenerate.setText(_translate("Dialog", "生成"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

